package sn.mfs.loyaltypoints;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {

        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("sn.mfs.loyaltypoints");

        noClasses()
            .that()
                .resideInAnyPackage("sn.mfs.loyaltypoints.service..")
            .or()
                .resideInAnyPackage("sn.mfs.loyaltypoints.repository..")
            .should().dependOnClassesThat()
                .resideInAnyPackage("..sn.mfs.loyaltypoints.web..")
        .because("Services and repositories should not depend on web layer")
        .check(importedClasses);
    }
}
