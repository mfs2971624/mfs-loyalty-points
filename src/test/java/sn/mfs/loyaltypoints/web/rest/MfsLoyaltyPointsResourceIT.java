package sn.mfs.loyaltypoints.web.rest;

import sn.mfs.loyaltypoints.MfsLoyaltyPointsApp;
import sn.mfs.loyaltypoints.domain.MfsLoyaltyPoints;
import sn.mfs.loyaltypoints.repository.MfsLoyaltyPointsRepository;
import sn.mfs.loyaltypoints.service.MfsLoyaltyPointsService;
import sn.mfs.loyaltypoints.service.dto.MfsLoyaltyPointsDTO;
import sn.mfs.loyaltypoints.service.mapper.MfsLoyaltyPointsMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link MfsLoyaltyPointsResource} REST controller.
 */
@SpringBootTest(classes = MfsLoyaltyPointsApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class MfsLoyaltyPointsResourceIT {

    private static final String DEFAULT_CUSTOMER_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_CUSTOMER_MSISDN = "BBBBBBBBBB";

    private static final String DEFAULT_TRX_ID = "AAAAAAAAAA";
    private static final String UPDATED_TRX_ID = "BBBBBBBBBB";

    private static final Integer DEFAULT_POINTS_EARNED = 1;
    private static final Integer UPDATED_POINTS_EARNED = 2;

    private static final Integer DEFAULT_POINTS_USED = 1;
    private static final Integer UPDATED_POINTS_USED = 2;

    private static final Boolean DEFAULT_IS_RESET = false;
    private static final Boolean UPDATED_IS_RESET = true;

    private static final Integer DEFAULT_PREVIOUS_BALANCE = 1;
    private static final Integer UPDATED_PREVIOUS_BALANCE = 2;

    private static final Integer DEFAULT_CURRENT_BALANCE = 1;
    private static final Integer UPDATED_CURRENT_BALANCE = 2;

    @Autowired
    private MfsLoyaltyPointsRepository mfsLoyaltyPointsRepository;

    @Autowired
    private MfsLoyaltyPointsMapper mfsLoyaltyPointsMapper;

    @Autowired
    private MfsLoyaltyPointsService mfsLoyaltyPointsService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMfsLoyaltyPointsMockMvc;

    private MfsLoyaltyPoints mfsLoyaltyPoints;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MfsLoyaltyPoints createEntity(EntityManager em) {
        MfsLoyaltyPoints mfsLoyaltyPoints = new MfsLoyaltyPoints()
            .customerMsisdn(DEFAULT_CUSTOMER_MSISDN)
            .trxId(DEFAULT_TRX_ID)
            .pointsEarned(DEFAULT_POINTS_EARNED)
            .pointsUsed(DEFAULT_POINTS_USED)
            .isReset(DEFAULT_IS_RESET)
            .previousBalance(DEFAULT_PREVIOUS_BALANCE)
            .currentBalance(DEFAULT_CURRENT_BALANCE);
        return mfsLoyaltyPoints;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MfsLoyaltyPoints createUpdatedEntity(EntityManager em) {
        MfsLoyaltyPoints mfsLoyaltyPoints = new MfsLoyaltyPoints()
            .customerMsisdn(UPDATED_CUSTOMER_MSISDN)
            .trxId(UPDATED_TRX_ID)
            .pointsEarned(UPDATED_POINTS_EARNED)
            .pointsUsed(UPDATED_POINTS_USED)
            .isReset(UPDATED_IS_RESET)
            .previousBalance(UPDATED_PREVIOUS_BALANCE)
            .currentBalance(UPDATED_CURRENT_BALANCE);
        return mfsLoyaltyPoints;
    }

    @BeforeEach
    public void initTest() {
        mfsLoyaltyPoints = createEntity(em);
    }

    @Test
    @Transactional
    public void createMfsLoyaltyPoints() throws Exception {
        int databaseSizeBeforeCreate = mfsLoyaltyPointsRepository.findAll().size();
        // Create the MfsLoyaltyPoints
        MfsLoyaltyPointsDTO mfsLoyaltyPointsDTO = mfsLoyaltyPointsMapper.toDto(mfsLoyaltyPoints);
        restMfsLoyaltyPointsMockMvc.perform(post("/api/mfs-loyalty-points")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(mfsLoyaltyPointsDTO)))
            .andExpect(status().isCreated());

        // Validate the MfsLoyaltyPoints in the database
        List<MfsLoyaltyPoints> mfsLoyaltyPointsList = mfsLoyaltyPointsRepository.findAll();
        assertThat(mfsLoyaltyPointsList).hasSize(databaseSizeBeforeCreate + 1);
        MfsLoyaltyPoints testMfsLoyaltyPoints = mfsLoyaltyPointsList.get(mfsLoyaltyPointsList.size() - 1);
        assertThat(testMfsLoyaltyPoints.getCustomerMsisdn()).isEqualTo(DEFAULT_CUSTOMER_MSISDN);
        assertThat(testMfsLoyaltyPoints.getTrxId()).isEqualTo(DEFAULT_TRX_ID);
        assertThat(testMfsLoyaltyPoints.getPointsEarned()).isEqualTo(DEFAULT_POINTS_EARNED);
        assertThat(testMfsLoyaltyPoints.getPointsUsed()).isEqualTo(DEFAULT_POINTS_USED);
        assertThat(testMfsLoyaltyPoints.isIsReset()).isEqualTo(DEFAULT_IS_RESET);
        assertThat(testMfsLoyaltyPoints.getPreviousBalance()).isEqualTo(DEFAULT_PREVIOUS_BALANCE);
        assertThat(testMfsLoyaltyPoints.getCurrentBalance()).isEqualTo(DEFAULT_CURRENT_BALANCE);
    }

    @Test
    @Transactional
    public void createMfsLoyaltyPointsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = mfsLoyaltyPointsRepository.findAll().size();

        // Create the MfsLoyaltyPoints with an existing ID
        mfsLoyaltyPoints.setId(1L);
        MfsLoyaltyPointsDTO mfsLoyaltyPointsDTO = mfsLoyaltyPointsMapper.toDto(mfsLoyaltyPoints);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMfsLoyaltyPointsMockMvc.perform(post("/api/mfs-loyalty-points")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(mfsLoyaltyPointsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the MfsLoyaltyPoints in the database
        List<MfsLoyaltyPoints> mfsLoyaltyPointsList = mfsLoyaltyPointsRepository.findAll();
        assertThat(mfsLoyaltyPointsList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkCustomerMsisdnIsRequired() throws Exception {
        int databaseSizeBeforeTest = mfsLoyaltyPointsRepository.findAll().size();
        // set the field null
        mfsLoyaltyPoints.setCustomerMsisdn(null);

        // Create the MfsLoyaltyPoints, which fails.
        MfsLoyaltyPointsDTO mfsLoyaltyPointsDTO = mfsLoyaltyPointsMapper.toDto(mfsLoyaltyPoints);


        restMfsLoyaltyPointsMockMvc.perform(post("/api/mfs-loyalty-points")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(mfsLoyaltyPointsDTO)))
            .andExpect(status().isBadRequest());

        List<MfsLoyaltyPoints> mfsLoyaltyPointsList = mfsLoyaltyPointsRepository.findAll();
        assertThat(mfsLoyaltyPointsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTrxIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = mfsLoyaltyPointsRepository.findAll().size();
        // set the field null
        mfsLoyaltyPoints.setTrxId(null);

        // Create the MfsLoyaltyPoints, which fails.
        MfsLoyaltyPointsDTO mfsLoyaltyPointsDTO = mfsLoyaltyPointsMapper.toDto(mfsLoyaltyPoints);


        restMfsLoyaltyPointsMockMvc.perform(post("/api/mfs-loyalty-points")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(mfsLoyaltyPointsDTO)))
            .andExpect(status().isBadRequest());

        List<MfsLoyaltyPoints> mfsLoyaltyPointsList = mfsLoyaltyPointsRepository.findAll();
        assertThat(mfsLoyaltyPointsList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMfsLoyaltyPoints() throws Exception {
        // Initialize the database
        mfsLoyaltyPointsRepository.saveAndFlush(mfsLoyaltyPoints);

        // Get all the mfsLoyaltyPointsList
        restMfsLoyaltyPointsMockMvc.perform(get("/api/mfs-loyalty-points?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(mfsLoyaltyPoints.getId().intValue())))
            .andExpect(jsonPath("$.[*].customerMsisdn").value(hasItem(DEFAULT_CUSTOMER_MSISDN)))
            .andExpect(jsonPath("$.[*].trxId").value(hasItem(DEFAULT_TRX_ID)))
            .andExpect(jsonPath("$.[*].pointsEarned").value(hasItem(DEFAULT_POINTS_EARNED)))
            .andExpect(jsonPath("$.[*].pointsUsed").value(hasItem(DEFAULT_POINTS_USED)))
            .andExpect(jsonPath("$.[*].isReset").value(hasItem(DEFAULT_IS_RESET.booleanValue())))
            .andExpect(jsonPath("$.[*].previousBalance").value(hasItem(DEFAULT_PREVIOUS_BALANCE)))
            .andExpect(jsonPath("$.[*].currentBalance").value(hasItem(DEFAULT_CURRENT_BALANCE)));
    }
    
    @Test
    @Transactional
    public void getMfsLoyaltyPoints() throws Exception {
        // Initialize the database
        mfsLoyaltyPointsRepository.saveAndFlush(mfsLoyaltyPoints);

        // Get the mfsLoyaltyPoints
        restMfsLoyaltyPointsMockMvc.perform(get("/api/mfs-loyalty-points/{id}", mfsLoyaltyPoints.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(mfsLoyaltyPoints.getId().intValue()))
            .andExpect(jsonPath("$.customerMsisdn").value(DEFAULT_CUSTOMER_MSISDN))
            .andExpect(jsonPath("$.trxId").value(DEFAULT_TRX_ID))
            .andExpect(jsonPath("$.pointsEarned").value(DEFAULT_POINTS_EARNED))
            .andExpect(jsonPath("$.pointsUsed").value(DEFAULT_POINTS_USED))
            .andExpect(jsonPath("$.isReset").value(DEFAULT_IS_RESET.booleanValue()))
            .andExpect(jsonPath("$.previousBalance").value(DEFAULT_PREVIOUS_BALANCE))
            .andExpect(jsonPath("$.currentBalance").value(DEFAULT_CURRENT_BALANCE));
    }
    @Test
    @Transactional
    public void getNonExistingMfsLoyaltyPoints() throws Exception {
        // Get the mfsLoyaltyPoints
        restMfsLoyaltyPointsMockMvc.perform(get("/api/mfs-loyalty-points/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMfsLoyaltyPoints() throws Exception {
        // Initialize the database
        mfsLoyaltyPointsRepository.saveAndFlush(mfsLoyaltyPoints);

        int databaseSizeBeforeUpdate = mfsLoyaltyPointsRepository.findAll().size();

        // Update the mfsLoyaltyPoints
        MfsLoyaltyPoints updatedMfsLoyaltyPoints = mfsLoyaltyPointsRepository.findById(mfsLoyaltyPoints.getId()).get();
        // Disconnect from session so that the updates on updatedMfsLoyaltyPoints are not directly saved in db
        em.detach(updatedMfsLoyaltyPoints);
        updatedMfsLoyaltyPoints
            .customerMsisdn(UPDATED_CUSTOMER_MSISDN)
            .trxId(UPDATED_TRX_ID)
            .pointsEarned(UPDATED_POINTS_EARNED)
            .pointsUsed(UPDATED_POINTS_USED)
            .isReset(UPDATED_IS_RESET)
            .previousBalance(UPDATED_PREVIOUS_BALANCE)
            .currentBalance(UPDATED_CURRENT_BALANCE);
        MfsLoyaltyPointsDTO mfsLoyaltyPointsDTO = mfsLoyaltyPointsMapper.toDto(updatedMfsLoyaltyPoints);

        restMfsLoyaltyPointsMockMvc.perform(put("/api/mfs-loyalty-points")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(mfsLoyaltyPointsDTO)))
            .andExpect(status().isOk());

        // Validate the MfsLoyaltyPoints in the database
        List<MfsLoyaltyPoints> mfsLoyaltyPointsList = mfsLoyaltyPointsRepository.findAll();
        assertThat(mfsLoyaltyPointsList).hasSize(databaseSizeBeforeUpdate);
        MfsLoyaltyPoints testMfsLoyaltyPoints = mfsLoyaltyPointsList.get(mfsLoyaltyPointsList.size() - 1);
        assertThat(testMfsLoyaltyPoints.getCustomerMsisdn()).isEqualTo(UPDATED_CUSTOMER_MSISDN);
        assertThat(testMfsLoyaltyPoints.getTrxId()).isEqualTo(UPDATED_TRX_ID);
        assertThat(testMfsLoyaltyPoints.getPointsEarned()).isEqualTo(UPDATED_POINTS_EARNED);
        assertThat(testMfsLoyaltyPoints.getPointsUsed()).isEqualTo(UPDATED_POINTS_USED);
        assertThat(testMfsLoyaltyPoints.isIsReset()).isEqualTo(UPDATED_IS_RESET);
        assertThat(testMfsLoyaltyPoints.getPreviousBalance()).isEqualTo(UPDATED_PREVIOUS_BALANCE);
        assertThat(testMfsLoyaltyPoints.getCurrentBalance()).isEqualTo(UPDATED_CURRENT_BALANCE);
    }

    @Test
    @Transactional
    public void updateNonExistingMfsLoyaltyPoints() throws Exception {
        int databaseSizeBeforeUpdate = mfsLoyaltyPointsRepository.findAll().size();

        // Create the MfsLoyaltyPoints
        MfsLoyaltyPointsDTO mfsLoyaltyPointsDTO = mfsLoyaltyPointsMapper.toDto(mfsLoyaltyPoints);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMfsLoyaltyPointsMockMvc.perform(put("/api/mfs-loyalty-points")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(mfsLoyaltyPointsDTO)))
            .andExpect(status().isBadRequest());

        // Validate the MfsLoyaltyPoints in the database
        List<MfsLoyaltyPoints> mfsLoyaltyPointsList = mfsLoyaltyPointsRepository.findAll();
        assertThat(mfsLoyaltyPointsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMfsLoyaltyPoints() throws Exception {
        // Initialize the database
        mfsLoyaltyPointsRepository.saveAndFlush(mfsLoyaltyPoints);

        int databaseSizeBeforeDelete = mfsLoyaltyPointsRepository.findAll().size();

        // Delete the mfsLoyaltyPoints
        restMfsLoyaltyPointsMockMvc.perform(delete("/api/mfs-loyalty-points/{id}", mfsLoyaltyPoints.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<MfsLoyaltyPoints> mfsLoyaltyPointsList = mfsLoyaltyPointsRepository.findAll();
        assertThat(mfsLoyaltyPointsList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
