package sn.mfs.loyaltypoints.domain;

import org.junit.jupiter.api.Test;
import sn.mfs.loyaltypoints.web.rest.TestUtil;

import static org.assertj.core.api.Assertions.assertThat;

public class MfsLoyaltyPointsTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MfsLoyaltyPoints.class);
        MfsLoyaltyPoints mfsLoyaltyPoints1 = new MfsLoyaltyPoints();
        mfsLoyaltyPoints1.setId(1L);
        MfsLoyaltyPoints mfsLoyaltyPoints2 = new MfsLoyaltyPoints();
        mfsLoyaltyPoints2.setId(mfsLoyaltyPoints1.getId());
        assertThat(mfsLoyaltyPoints1).isEqualTo(mfsLoyaltyPoints2);
        mfsLoyaltyPoints2.setId(2L);
        assertThat(mfsLoyaltyPoints1).isNotEqualTo(mfsLoyaltyPoints2);
        mfsLoyaltyPoints1.setId(null);
        assertThat(mfsLoyaltyPoints1).isNotEqualTo(mfsLoyaltyPoints2);
    }
}
