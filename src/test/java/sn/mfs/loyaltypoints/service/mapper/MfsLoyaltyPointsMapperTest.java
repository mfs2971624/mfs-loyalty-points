package sn.mfs.loyaltypoints.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class MfsLoyaltyPointsMapperTest {

    private MfsLoyaltyPointsMapper mfsLoyaltyPointsMapper;

    @BeforeEach
    public void setUp() {
        mfsLoyaltyPointsMapper = new MfsLoyaltyPointsMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(mfsLoyaltyPointsMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(mfsLoyaltyPointsMapper.fromId(null)).isNull();
    }
}
