package sn.mfs.loyaltypoints.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import sn.mfs.loyaltypoints.web.rest.TestUtil;

public class MfsLoyaltyPointsDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(MfsLoyaltyPointsDTO.class);
        MfsLoyaltyPointsDTO mfsLoyaltyPointsDTO1 = new MfsLoyaltyPointsDTO();
        mfsLoyaltyPointsDTO1.setId(1L);
        MfsLoyaltyPointsDTO mfsLoyaltyPointsDTO2 = new MfsLoyaltyPointsDTO();
        assertThat(mfsLoyaltyPointsDTO1).isNotEqualTo(mfsLoyaltyPointsDTO2);
        mfsLoyaltyPointsDTO2.setId(mfsLoyaltyPointsDTO1.getId());
        assertThat(mfsLoyaltyPointsDTO1).isEqualTo(mfsLoyaltyPointsDTO2);
        mfsLoyaltyPointsDTO2.setId(2L);
        assertThat(mfsLoyaltyPointsDTO1).isNotEqualTo(mfsLoyaltyPointsDTO2);
        mfsLoyaltyPointsDTO1.setId(null);
        assertThat(mfsLoyaltyPointsDTO1).isNotEqualTo(mfsLoyaltyPointsDTO2);
    }
}
