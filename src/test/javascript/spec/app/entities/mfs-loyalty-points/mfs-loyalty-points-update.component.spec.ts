import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { MfsLoyaltyPointsTestModule } from '../../../test.module';
import { MfsLoyaltyPointsUpdateComponent } from 'app/entities/mfs-loyalty-points/mfs-loyalty-points-update.component';
import { MfsLoyaltyPointsService } from 'app/entities/mfs-loyalty-points/mfs-loyalty-points.service';
import { MfsLoyaltyPoints } from 'app/shared/model/mfs-loyalty-points.model';

describe('Component Tests', () => {
  describe('MfsLoyaltyPoints Management Update Component', () => {
    let comp: MfsLoyaltyPointsUpdateComponent;
    let fixture: ComponentFixture<MfsLoyaltyPointsUpdateComponent>;
    let service: MfsLoyaltyPointsService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MfsLoyaltyPointsTestModule],
        declarations: [MfsLoyaltyPointsUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(MfsLoyaltyPointsUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MfsLoyaltyPointsUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MfsLoyaltyPointsService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new MfsLoyaltyPoints(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new MfsLoyaltyPoints();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
