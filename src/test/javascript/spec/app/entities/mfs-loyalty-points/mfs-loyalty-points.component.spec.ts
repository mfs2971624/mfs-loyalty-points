import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, convertToParamMap } from '@angular/router';

import { MfsLoyaltyPointsTestModule } from '../../../test.module';
import { MfsLoyaltyPointsComponent } from 'app/entities/mfs-loyalty-points/mfs-loyalty-points.component';
import { MfsLoyaltyPointsService } from 'app/entities/mfs-loyalty-points/mfs-loyalty-points.service';
import { MfsLoyaltyPoints } from 'app/shared/model/mfs-loyalty-points.model';

describe('Component Tests', () => {
  describe('MfsLoyaltyPoints Management Component', () => {
    let comp: MfsLoyaltyPointsComponent;
    let fixture: ComponentFixture<MfsLoyaltyPointsComponent>;
    let service: MfsLoyaltyPointsService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MfsLoyaltyPointsTestModule],
        declarations: [MfsLoyaltyPointsComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: {
              data: of({
                defaultSort: 'id,asc',
              }),
              queryParamMap: of(
                convertToParamMap({
                  page: '1',
                  size: '1',
                  sort: 'id,desc',
                })
              ),
            },
          },
        ],
      })
        .overrideTemplate(MfsLoyaltyPointsComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MfsLoyaltyPointsComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MfsLoyaltyPointsService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new MfsLoyaltyPoints(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.mfsLoyaltyPoints && comp.mfsLoyaltyPoints[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should load a page', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new MfsLoyaltyPoints(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.loadPage(1);

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.mfsLoyaltyPoints && comp.mfsLoyaltyPoints[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      comp.ngOnInit();
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,desc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // INIT
      comp.ngOnInit();

      // GIVEN
      comp.predicate = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,desc', 'id']);
    });
  });
});
