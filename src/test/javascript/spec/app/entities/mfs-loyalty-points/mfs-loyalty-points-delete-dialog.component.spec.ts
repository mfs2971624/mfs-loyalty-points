import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { MfsLoyaltyPointsTestModule } from '../../../test.module';
import { MockEventManager } from '../../../helpers/mock-event-manager.service';
import { MockActiveModal } from '../../../helpers/mock-active-modal.service';
import { MfsLoyaltyPointsDeleteDialogComponent } from 'app/entities/mfs-loyalty-points/mfs-loyalty-points-delete-dialog.component';
import { MfsLoyaltyPointsService } from 'app/entities/mfs-loyalty-points/mfs-loyalty-points.service';

describe('Component Tests', () => {
  describe('MfsLoyaltyPoints Management Delete Component', () => {
    let comp: MfsLoyaltyPointsDeleteDialogComponent;
    let fixture: ComponentFixture<MfsLoyaltyPointsDeleteDialogComponent>;
    let service: MfsLoyaltyPointsService;
    let mockEventManager: MockEventManager;
    let mockActiveModal: MockActiveModal;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MfsLoyaltyPointsTestModule],
        declarations: [MfsLoyaltyPointsDeleteDialogComponent],
      })
        .overrideTemplate(MfsLoyaltyPointsDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MfsLoyaltyPointsDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MfsLoyaltyPointsService);
      mockEventManager = TestBed.get(JhiEventManager);
      mockActiveModal = TestBed.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.closeSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));

      it('Should not call delete service on clear', () => {
        // GIVEN
        spyOn(service, 'delete');

        // WHEN
        comp.cancel();

        // THEN
        expect(service.delete).not.toHaveBeenCalled();
        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
      });
    });
  });
});
