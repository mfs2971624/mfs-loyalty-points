import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MfsLoyaltyPointsTestModule } from '../../../test.module';
import { MfsLoyaltyPointsDetailComponent } from 'app/entities/mfs-loyalty-points/mfs-loyalty-points-detail.component';
import { MfsLoyaltyPoints } from 'app/shared/model/mfs-loyalty-points.model';

describe('Component Tests', () => {
  describe('MfsLoyaltyPoints Management Detail Component', () => {
    let comp: MfsLoyaltyPointsDetailComponent;
    let fixture: ComponentFixture<MfsLoyaltyPointsDetailComponent>;
    const route = ({ data: of({ mfsLoyaltyPoints: new MfsLoyaltyPoints(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [MfsLoyaltyPointsTestModule],
        declarations: [MfsLoyaltyPointsDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(MfsLoyaltyPointsDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MfsLoyaltyPointsDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load mfsLoyaltyPoints on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.mfsLoyaltyPoints).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
