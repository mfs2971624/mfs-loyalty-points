import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { MfsLoyaltyPointsService } from 'app/entities/mfs-loyalty-points/mfs-loyalty-points.service';
import { IMfsLoyaltyPoints, MfsLoyaltyPoints } from 'app/shared/model/mfs-loyalty-points.model';

describe('Service Tests', () => {
  describe('MfsLoyaltyPoints Service', () => {
    let injector: TestBed;
    let service: MfsLoyaltyPointsService;
    let httpMock: HttpTestingController;
    let elemDefault: IMfsLoyaltyPoints;
    let expectedResult: IMfsLoyaltyPoints | IMfsLoyaltyPoints[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(MfsLoyaltyPointsService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new MfsLoyaltyPoints(0, 'AAAAAAA', 'AAAAAAA', 0, 0, false, 0, 0);
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a MfsLoyaltyPoints', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new MfsLoyaltyPoints()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a MfsLoyaltyPoints', () => {
        const returnedFromService = Object.assign(
          {
            customerMsisdn: 'BBBBBB',
            trxId: 'BBBBBB',
            pointsEarned: 1,
            pointsUsed: 1,
            isReset: true,
            previousBalance: 1,
            currentBalance: 1,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of MfsLoyaltyPoints', () => {
        const returnedFromService = Object.assign(
          {
            customerMsisdn: 'BBBBBB',
            trxId: 'BBBBBB',
            pointsEarned: 1,
            pointsUsed: 1,
            isReset: true,
            previousBalance: 1,
            currentBalance: 1,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a MfsLoyaltyPoints', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
