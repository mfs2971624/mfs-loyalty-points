package sn.mfs.loyaltypoints.domain.enumeration;

public enum MfsLoyaltyPointsRequestType {
    ADD, SUBTRACT, RESET
}
