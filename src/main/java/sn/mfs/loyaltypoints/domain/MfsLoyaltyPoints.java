package sn.mfs.loyaltypoints.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A MfsLoyaltyPoints.
 */
@Entity
@Table(name = "mfs_loyalty_points")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class MfsLoyaltyPoints implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "customer_msisdn", nullable = false)
    private String customerMsisdn;

    @NotNull
    @Column(name = "trx_id", nullable = false)
    private String trxId;

    @Column(name = "points_earned")
    private Integer pointsEarned;

    @Column(name = "points_used")
    private Integer pointsUsed;

    @Column(name = "is_reset")
    private Boolean isReset;

    @Column(name = "previous_balance")
    private Integer previousBalance;

    @Column(name = "current_balance")
    private Integer currentBalance;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCustomerMsisdn() {
        return customerMsisdn;
    }

    public MfsLoyaltyPoints customerMsisdn(String customerMsisdn) {
        this.customerMsisdn = customerMsisdn;
        return this;
    }

    public void setCustomerMsisdn(String customerMsisdn) {
        this.customerMsisdn = customerMsisdn;
    }

    public String getTrxId() {
        return trxId;
    }

    public MfsLoyaltyPoints trxId(String trxId) {
        this.trxId = trxId;
        return this;
    }

    public void setTrxId(String trxId) {
        this.trxId = trxId;
    }

    public Integer getPointsEarned() {
        return pointsEarned;
    }

    public MfsLoyaltyPoints pointsEarned(Integer pointsEarned) {
        this.pointsEarned = pointsEarned;
        return this;
    }

    public void setPointsEarned(Integer pointsEarned) {
        this.pointsEarned = pointsEarned;
    }

    public Integer getPointsUsed() {
        return pointsUsed;
    }

    public MfsLoyaltyPoints pointsUsed(Integer pointsUsed) {
        this.pointsUsed = pointsUsed;
        return this;
    }

    public void setPointsUsed(Integer pointsUsed) {
        this.pointsUsed = pointsUsed;
    }

    public Boolean isIsReset() {
        return isReset;
    }

    public MfsLoyaltyPoints isReset(Boolean isReset) {
        this.isReset = isReset;
        return this;
    }

    public void setIsReset(Boolean isReset) {
        this.isReset = isReset;
    }

    public Integer getPreviousBalance() {
        return previousBalance;
    }

    public MfsLoyaltyPoints previousBalance(Integer previousBalance) {
        this.previousBalance = previousBalance;
        return this;
    }

    public void setPreviousBalance(Integer previousBalance) {
        this.previousBalance = previousBalance;
    }

    public Integer getCurrentBalance() {
        return currentBalance;
    }

    public MfsLoyaltyPoints currentBalance(Integer currentBalance) {
        this.currentBalance = currentBalance;
        return this;
    }

    public void setCurrentBalance(Integer currentBalance) {
        this.currentBalance = currentBalance;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MfsLoyaltyPoints)) {
            return false;
        }
        return id != null && id.equals(((MfsLoyaltyPoints) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MfsLoyaltyPoints{" +
            "id=" + getId() +
            ", customerMsisdn='" + getCustomerMsisdn() + "'" +
            ", trxId='" + getTrxId() + "'" +
            ", pointsEarned=" + getPointsEarned() +
            ", pointsUsed=" + getPointsUsed() +
            ", isReset='" + isIsReset() + "'" +
            ", previousBalance=" + getPreviousBalance() +
            ", currentBalance=" + getCurrentBalance() +
            "}";
    }
}
