package sn.mfs.loyaltypoints.domain.request;

import sn.mfs.loyaltypoints.domain.enumeration.MfsLoyaltyPointsRequestType;

import java.util.Objects;

public class MfsLoyaltyPointsRequest {
    private String trxId;
    private String customerMsisdn;
    private MfsLoyaltyPointsRequestType type;
    private int points;

    public MfsLoyaltyPointsRequest() {
    }

    public MfsLoyaltyPointsRequest(String trxId, String customerMsisdn, MfsLoyaltyPointsRequestType type, int points) {
        this.trxId = trxId;
        this.customerMsisdn = customerMsisdn;
        this.type = type;
        this.points = points;
    }

    public String getTrxId() {
        return trxId;
    }

    public void setTrxId(String trxId) {
        this.trxId = trxId;
    }

    public String getCustomerMsisdn() {
        return customerMsisdn;
    }

    public void setCustomerMsisdn(String customerMsisdn) {
        this.customerMsisdn = customerMsisdn;
    }

    public MfsLoyaltyPointsRequestType getType() {
        return type;
    }

    public void setType(MfsLoyaltyPointsRequestType type) {
        this.type = type;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MfsLoyaltyPointsRequest that = (MfsLoyaltyPointsRequest) o;
        return points == that.points &&
            Objects.equals(trxId, that.trxId) &&
            Objects.equals(customerMsisdn, that.customerMsisdn) &&
            type == that.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(trxId, customerMsisdn, type, points);
    }

    @Override
    public String toString() {
        return "MfsLoyaltyPointsRequest{" +
            "trxId='" + trxId + '\'' +
            ", customerMsisdn='" + customerMsisdn + '\'' +
            ", type=" + type +
            ", points=" + points +
            '}';
    }
}
