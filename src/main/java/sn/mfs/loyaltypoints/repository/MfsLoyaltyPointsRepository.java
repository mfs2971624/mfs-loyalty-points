package sn.mfs.loyaltypoints.repository;

import sn.mfs.loyaltypoints.domain.MfsLoyaltyPoints;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data  repository for the MfsLoyaltyPoints entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MfsLoyaltyPointsRepository extends JpaRepository<MfsLoyaltyPoints, Long> {
    Optional<MfsLoyaltyPoints> findFirstByCustomerMsisdnOrderByIdDesc(String customerMsisdn);
}
