package sn.mfs.loyaltypoints.service.mapper;


import sn.mfs.loyaltypoints.domain.*;
import sn.mfs.loyaltypoints.service.dto.MfsLoyaltyPointsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link MfsLoyaltyPoints} and its DTO {@link MfsLoyaltyPointsDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MfsLoyaltyPointsMapper extends EntityMapper<MfsLoyaltyPointsDTO, MfsLoyaltyPoints> {



    default MfsLoyaltyPoints fromId(Long id) {
        if (id == null) {
            return null;
        }
        MfsLoyaltyPoints mfsLoyaltyPoints = new MfsLoyaltyPoints();
        mfsLoyaltyPoints.setId(id);
        return mfsLoyaltyPoints;
    }
}
