package sn.mfs.loyaltypoints.service.impl;

import sn.mfs.loyaltypoints.domain.enumeration.MfsLoyaltyPointsRequestType;
import sn.mfs.loyaltypoints.domain.request.MfsLoyaltyPointsRequest;
import sn.mfs.loyaltypoints.service.MfsLoyaltyPointsService;
import sn.mfs.loyaltypoints.domain.MfsLoyaltyPoints;
import sn.mfs.loyaltypoints.repository.MfsLoyaltyPointsRepository;
import sn.mfs.loyaltypoints.service.dto.MfsLoyaltyPointsDTO;
import sn.mfs.loyaltypoints.service.mapper.MfsLoyaltyPointsMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link MfsLoyaltyPoints}.
 */
@Service
@Transactional
public class MfsLoyaltyPointsServiceImpl implements MfsLoyaltyPointsService {

    private final Logger log = LoggerFactory.getLogger(MfsLoyaltyPointsServiceImpl.class);

    private final MfsLoyaltyPointsRepository mfsLoyaltyPointsRepository;

    private final MfsLoyaltyPointsMapper mfsLoyaltyPointsMapper;

    public MfsLoyaltyPointsServiceImpl(MfsLoyaltyPointsRepository mfsLoyaltyPointsRepository, MfsLoyaltyPointsMapper mfsLoyaltyPointsMapper) {
        this.mfsLoyaltyPointsRepository = mfsLoyaltyPointsRepository;
        this.mfsLoyaltyPointsMapper = mfsLoyaltyPointsMapper;
    }

    @Override
    public MfsLoyaltyPointsDTO save(MfsLoyaltyPointsDTO mfsLoyaltyPointsDTO) {
        log.debug("Request to save MfsLoyaltyPoints : {}", mfsLoyaltyPointsDTO);
        MfsLoyaltyPoints mfsLoyaltyPoints = mfsLoyaltyPointsMapper.toEntity(mfsLoyaltyPointsDTO);
        mfsLoyaltyPoints = mfsLoyaltyPointsRepository.save(mfsLoyaltyPoints);
        return mfsLoyaltyPointsMapper.toDto(mfsLoyaltyPoints);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<MfsLoyaltyPointsDTO> findAll(Pageable pageable) {
        log.debug("Request to get all MfsLoyaltyPoints");
        return mfsLoyaltyPointsRepository.findAll(pageable)
            .map(mfsLoyaltyPointsMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<MfsLoyaltyPointsDTO> findOne(Long id) {
        log.debug("Request to get MfsLoyaltyPoints : {}", id);
        return mfsLoyaltyPointsRepository.findById(id)
            .map(mfsLoyaltyPointsMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<MfsLoyaltyPointsDTO> findLatestByCustomerMsisdn(String customerMsisdn) {
        log.debug("Request to get MfsLoyaltyPoints : {}", customerMsisdn);
        return mfsLoyaltyPointsRepository.findFirstByCustomerMsisdnOrderByIdDesc(customerMsisdn)
            .map(mfsLoyaltyPointsMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete MfsLoyaltyPoints : {}", id);
        mfsLoyaltyPointsRepository.deleteById(id);
    }

    @Override
    public MfsLoyaltyPointsDTO handle(MfsLoyaltyPointsRequest mfsLoyaltyPointsRequest) {
        MfsLoyaltyPointsDTO resultDTO = new MfsLoyaltyPointsDTO();
        resultDTO.setTrxId(mfsLoyaltyPointsRequest.getTrxId());
        resultDTO.setCustomerMsisdn(mfsLoyaltyPointsRequest.getCustomerMsisdn());
        Optional<MfsLoyaltyPointsDTO> previousLoyaltyPoints = findLatestByCustomerMsisdn(mfsLoyaltyPointsRequest.getCustomerMsisdn());
        if (previousLoyaltyPoints.isPresent()) {
            resultDTO.setPreviousBalance(previousLoyaltyPoints.get().getCurrentBalance());
        } else {
            resultDTO.setPreviousBalance(0);
        }

        if(mfsLoyaltyPointsRequest.getType().equals(MfsLoyaltyPointsRequestType.ADD)) {
            resultDTO.setPointsEarned(mfsLoyaltyPointsRequest.getPoints());
            resultDTO.setCurrentBalance(resultDTO.getPreviousBalance() + mfsLoyaltyPointsRequest.getPoints());
            resultDTO.setPointsUsed(0);
            resultDTO.setIsReset(false);
        }

        if(mfsLoyaltyPointsRequest.getType().equals(MfsLoyaltyPointsRequestType.SUBTRACT)) {
            resultDTO.setPointsUsed(mfsLoyaltyPointsRequest.getPoints());
            resultDTO.setCurrentBalance(resultDTO.getPreviousBalance() - mfsLoyaltyPointsRequest.getPoints());
            resultDTO.setPointsEarned(0);
            resultDTO.setIsReset(false);
        }

        if(mfsLoyaltyPointsRequest.getType().equals(MfsLoyaltyPointsRequestType.RESET)) {
            resultDTO.setCurrentBalance(0);
            resultDTO.setPointsUsed(0);
            resultDTO.setPointsEarned(0);
            resultDTO.setIsReset(true);
        }

        return save(resultDTO);
    }

}
