package sn.mfs.loyaltypoints.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link sn.mfs.loyaltypoints.domain.MfsLoyaltyPoints} entity.
 */
public class MfsLoyaltyPointsDTO implements Serializable {
    
    private Long id;

    @NotNull
    private String customerMsisdn;

    @NotNull
    private String trxId;

    private Integer pointsEarned;

    private Integer pointsUsed;

    private Boolean isReset;

    private Integer previousBalance;

    private Integer currentBalance;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCustomerMsisdn() {
        return customerMsisdn;
    }

    public void setCustomerMsisdn(String customerMsisdn) {
        this.customerMsisdn = customerMsisdn;
    }

    public String getTrxId() {
        return trxId;
    }

    public void setTrxId(String trxId) {
        this.trxId = trxId;
    }

    public Integer getPointsEarned() {
        return pointsEarned;
    }

    public void setPointsEarned(Integer pointsEarned) {
        this.pointsEarned = pointsEarned;
    }

    public Integer getPointsUsed() {
        return pointsUsed;
    }

    public void setPointsUsed(Integer pointsUsed) {
        this.pointsUsed = pointsUsed;
    }

    public Boolean isIsReset() {
        return isReset;
    }

    public void setIsReset(Boolean isReset) {
        this.isReset = isReset;
    }

    public Integer getPreviousBalance() {
        return previousBalance;
    }

    public void setPreviousBalance(Integer previousBalance) {
        this.previousBalance = previousBalance;
    }

    public Integer getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(Integer currentBalance) {
        this.currentBalance = currentBalance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MfsLoyaltyPointsDTO)) {
            return false;
        }

        return id != null && id.equals(((MfsLoyaltyPointsDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MfsLoyaltyPointsDTO{" +
            "id=" + getId() +
            ", customerMsisdn='" + getCustomerMsisdn() + "'" +
            ", trxId='" + getTrxId() + "'" +
            ", pointsEarned=" + getPointsEarned() +
            ", pointsUsed=" + getPointsUsed() +
            ", isReset='" + isIsReset() + "'" +
            ", previousBalance=" + getPreviousBalance() +
            ", currentBalance=" + getCurrentBalance() +
            "}";
    }
}
