package sn.mfs.loyaltypoints.service;

import sn.mfs.loyaltypoints.domain.MfsLoyaltyPoints;
import sn.mfs.loyaltypoints.domain.request.MfsLoyaltyPointsRequest;
import sn.mfs.loyaltypoints.service.dto.MfsLoyaltyPointsDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link sn.mfs.loyaltypoints.domain.MfsLoyaltyPoints}.
 */
public interface MfsLoyaltyPointsService {

    /**
     * Save a mfsLoyaltyPoints.
     *
     * @param mfsLoyaltyPointsDTO the entity to save.
     * @return the persisted entity.
     */
    MfsLoyaltyPointsDTO save(MfsLoyaltyPointsDTO mfsLoyaltyPointsDTO);

    /**
     * Get all the mfsLoyaltyPoints.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<MfsLoyaltyPointsDTO> findAll(Pageable pageable);


    /**
     * Get the "id" mfsLoyaltyPoints.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<MfsLoyaltyPointsDTO> findOne(Long id);

    Optional<MfsLoyaltyPointsDTO> findLatestByCustomerMsisdn(String customerMsisdn);

    /**
     * Delete the "id" mfsLoyaltyPoints.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    MfsLoyaltyPointsDTO handle(MfsLoyaltyPointsRequest mfsLoyaltyPointsRequest);
}
