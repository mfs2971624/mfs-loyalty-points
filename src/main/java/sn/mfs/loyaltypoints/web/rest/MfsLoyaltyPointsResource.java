package sn.mfs.loyaltypoints.web.rest;

import sn.mfs.loyaltypoints.domain.enumeration.MfsLoyaltyPointsRequestType;
import sn.mfs.loyaltypoints.domain.request.MfsLoyaltyPointsRequest;
import sn.mfs.loyaltypoints.service.MfsLoyaltyPointsService;
import sn.mfs.loyaltypoints.web.rest.errors.BadRequestAlertException;
import sn.mfs.loyaltypoints.service.dto.MfsLoyaltyPointsDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link sn.mfs.loyaltypoints.domain.MfsLoyaltyPoints}.
 */
@RestController
@RequestMapping("/api")
public class MfsLoyaltyPointsResource {

    private final Logger log = LoggerFactory.getLogger(MfsLoyaltyPointsResource.class);

    private static final String ENTITY_NAME = "mfsLoyaltyPoints";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MfsLoyaltyPointsService mfsLoyaltyPointsService;

    public MfsLoyaltyPointsResource(MfsLoyaltyPointsService mfsLoyaltyPointsService) {
        this.mfsLoyaltyPointsService = mfsLoyaltyPointsService;
    }

    /**
     * {@code POST  /mfs-loyalty-points} : Create a new mfsLoyaltyPoints.
     *
     * @param mfsLoyaltyPointsDTO the mfsLoyaltyPointsDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new mfsLoyaltyPointsDTO, or with status {@code 400 (Bad Request)} if the mfsLoyaltyPoints has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
//    @PostMapping("/mfs-loyalty-points")
//    public ResponseEntity<MfsLoyaltyPointsDTO> createMfsLoyaltyPoints(@Valid @RequestBody MfsLoyaltyPointsDTO mfsLoyaltyPointsDTO) throws URISyntaxException {
//        log.debug("REST request to save MfsLoyaltyPoints : {}", mfsLoyaltyPointsDTO);
//        if (mfsLoyaltyPointsDTO.getId() != null) {
//            throw new BadRequestAlertException("A new mfsLoyaltyPoints cannot already have an ID", ENTITY_NAME, "idexists");
//        }
//        MfsLoyaltyPointsDTO result = mfsLoyaltyPointsService.save(mfsLoyaltyPointsDTO);
//        return ResponseEntity.created(new URI("/api/mfs-loyalty-points/" + result.getId()))
//            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
//            .body(result);
//    }
    @PostMapping("/mfs-loyalty-points")
    public ResponseEntity<MfsLoyaltyPointsDTO> createMfsLoyaltyPoints(@Valid @RequestBody MfsLoyaltyPointsRequest mfsLoyaltyPointsRequest) throws URISyntaxException {
        log.debug("REST request to save MfsLoyaltyPoints : {}", mfsLoyaltyPointsRequest);
        MfsLoyaltyPointsDTO result = mfsLoyaltyPointsService.handle(mfsLoyaltyPointsRequest);

        return ResponseEntity.created(new URI("/api/mfs-loyalty-points/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /mfs-loyalty-points} : Updates an existing mfsLoyaltyPoints.
     *
     * @param mfsLoyaltyPointsDTO the mfsLoyaltyPointsDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated mfsLoyaltyPointsDTO,
     * or with status {@code 400 (Bad Request)} if the mfsLoyaltyPointsDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the mfsLoyaltyPointsDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/mfs-loyalty-points")
    public ResponseEntity<MfsLoyaltyPointsDTO> updateMfsLoyaltyPoints(@Valid @RequestBody MfsLoyaltyPointsDTO mfsLoyaltyPointsDTO) throws URISyntaxException {
        log.debug("REST request to update MfsLoyaltyPoints : {}", mfsLoyaltyPointsDTO);
        if (mfsLoyaltyPointsDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MfsLoyaltyPointsDTO result = mfsLoyaltyPointsService.save(mfsLoyaltyPointsDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, mfsLoyaltyPointsDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /mfs-loyalty-points} : get all the mfsLoyaltyPoints.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of mfsLoyaltyPoints in body.
     */
    @GetMapping("/mfs-loyalty-points")
    public ResponseEntity<List<MfsLoyaltyPointsDTO>> getAllMfsLoyaltyPoints(Pageable pageable) {
        log.debug("REST request to get a page of MfsLoyaltyPoints");
        Page<MfsLoyaltyPointsDTO> page = mfsLoyaltyPointsService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }


    /**
     * {@code GET  /mfs-loyalty-points/:id} : get the "id" mfsLoyaltyPoints.
     *
     * @param id the id of the mfsLoyaltyPointsDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the mfsLoyaltyPointsDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/mfs-loyalty-points/{id}")
    public ResponseEntity<MfsLoyaltyPointsDTO> getMfsLoyaltyPoints(@PathVariable Long id) {
        log.debug("REST request to get MfsLoyaltyPoints : {}", id);
        Optional<MfsLoyaltyPointsDTO> mfsLoyaltyPointsDTO = mfsLoyaltyPointsService.findOne(id);
        return ResponseUtil.wrapOrNotFound(mfsLoyaltyPointsDTO);
    }


    @GetMapping("/mfs-loyalty-points/balance")
    public ResponseEntity<MfsLoyaltyPointsDTO> getCustomerLoyaltyPointsBalance(@RequestParam String customerMsisdn) {
        log.debug("REST request to get current MfsLoyaltyPoints for msisdn {}", customerMsisdn);
        Optional<MfsLoyaltyPointsDTO> mfsLoyaltyPointsDTO = mfsLoyaltyPointsService.findLatestByCustomerMsisdn(customerMsisdn);
        return ResponseUtil.wrapOrNotFound(mfsLoyaltyPointsDTO);
    }

    /**
     * {@code DELETE  /mfs-loyalty-points/:id} : delete the "id" mfsLoyaltyPoints.
     *
     * @param id the id of the mfsLoyaltyPointsDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/mfs-loyalty-points/{id}")
    public ResponseEntity<Void> deleteMfsLoyaltyPoints(@PathVariable Long id) {
        log.debug("REST request to delete MfsLoyaltyPoints : {}", id);
        mfsLoyaltyPointsService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
