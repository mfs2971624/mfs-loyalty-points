import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'mfs-loyalty-points',
        loadChildren: () => import('./mfs-loyalty-points/mfs-loyalty-points.module').then(m => m.MfsLoyaltyPointsMfsLoyaltyPointsModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class MfsLoyaltyPointsEntityModule {}
