import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IMfsLoyaltyPoints } from 'app/shared/model/mfs-loyalty-points.model';
import { MfsLoyaltyPointsService } from './mfs-loyalty-points.service';

@Component({
  templateUrl: './mfs-loyalty-points-delete-dialog.component.html',
})
export class MfsLoyaltyPointsDeleteDialogComponent {
  mfsLoyaltyPoints?: IMfsLoyaltyPoints;

  constructor(
    protected mfsLoyaltyPointsService: MfsLoyaltyPointsService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.mfsLoyaltyPointsService.delete(id).subscribe(() => {
      this.eventManager.broadcast('mfsLoyaltyPointsListModification');
      this.activeModal.close();
    });
  }
}
