import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IMfsLoyaltyPoints, MfsLoyaltyPoints } from 'app/shared/model/mfs-loyalty-points.model';
import { MfsLoyaltyPointsService } from './mfs-loyalty-points.service';
import { MfsLoyaltyPointsComponent } from './mfs-loyalty-points.component';
import { MfsLoyaltyPointsDetailComponent } from './mfs-loyalty-points-detail.component';
import { MfsLoyaltyPointsUpdateComponent } from './mfs-loyalty-points-update.component';

@Injectable({ providedIn: 'root' })
export class MfsLoyaltyPointsResolve implements Resolve<IMfsLoyaltyPoints> {
  constructor(private service: MfsLoyaltyPointsService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IMfsLoyaltyPoints> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((mfsLoyaltyPoints: HttpResponse<MfsLoyaltyPoints>) => {
          if (mfsLoyaltyPoints.body) {
            return of(mfsLoyaltyPoints.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new MfsLoyaltyPoints());
  }
}

export const mfsLoyaltyPointsRoute: Routes = [
  {
    path: '',
    component: MfsLoyaltyPointsComponent,
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'mfsLoyaltyPointsApp.mfsLoyaltyPoints.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: MfsLoyaltyPointsDetailComponent,
    resolve: {
      mfsLoyaltyPoints: MfsLoyaltyPointsResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'mfsLoyaltyPointsApp.mfsLoyaltyPoints.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: MfsLoyaltyPointsUpdateComponent,
    resolve: {
      mfsLoyaltyPoints: MfsLoyaltyPointsResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'mfsLoyaltyPointsApp.mfsLoyaltyPoints.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: MfsLoyaltyPointsUpdateComponent,
    resolve: {
      mfsLoyaltyPoints: MfsLoyaltyPointsResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'mfsLoyaltyPointsApp.mfsLoyaltyPoints.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
