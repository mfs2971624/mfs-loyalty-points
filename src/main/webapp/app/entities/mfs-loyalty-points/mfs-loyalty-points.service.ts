import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IMfsLoyaltyPoints } from 'app/shared/model/mfs-loyalty-points.model';

type EntityResponseType = HttpResponse<IMfsLoyaltyPoints>;
type EntityArrayResponseType = HttpResponse<IMfsLoyaltyPoints[]>;

@Injectable({ providedIn: 'root' })
export class MfsLoyaltyPointsService {
  public resourceUrl = SERVER_API_URL + 'api/mfs-loyalty-points';

  constructor(protected http: HttpClient) {}

  create(mfsLoyaltyPoints: IMfsLoyaltyPoints): Observable<EntityResponseType> {
    return this.http.post<IMfsLoyaltyPoints>(this.resourceUrl, mfsLoyaltyPoints, { observe: 'response' });
  }

  update(mfsLoyaltyPoints: IMfsLoyaltyPoints): Observable<EntityResponseType> {
    return this.http.put<IMfsLoyaltyPoints>(this.resourceUrl, mfsLoyaltyPoints, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IMfsLoyaltyPoints>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IMfsLoyaltyPoints[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
