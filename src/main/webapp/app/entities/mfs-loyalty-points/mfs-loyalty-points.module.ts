import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MfsLoyaltyPointsSharedModule } from 'app/shared/shared.module';
import { MfsLoyaltyPointsComponent } from './mfs-loyalty-points.component';
import { MfsLoyaltyPointsDetailComponent } from './mfs-loyalty-points-detail.component';
import { MfsLoyaltyPointsUpdateComponent } from './mfs-loyalty-points-update.component';
import { MfsLoyaltyPointsDeleteDialogComponent } from './mfs-loyalty-points-delete-dialog.component';
import { mfsLoyaltyPointsRoute } from './mfs-loyalty-points.route';

@NgModule({
  imports: [MfsLoyaltyPointsSharedModule, RouterModule.forChild(mfsLoyaltyPointsRoute)],
  declarations: [
    MfsLoyaltyPointsComponent,
    MfsLoyaltyPointsDetailComponent,
    MfsLoyaltyPointsUpdateComponent,
    MfsLoyaltyPointsDeleteDialogComponent,
  ],
  entryComponents: [MfsLoyaltyPointsDeleteDialogComponent],
})
export class MfsLoyaltyPointsMfsLoyaltyPointsModule {}
