import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMfsLoyaltyPoints } from 'app/shared/model/mfs-loyalty-points.model';

@Component({
  selector: 'jhi-mfs-loyalty-points-detail',
  templateUrl: './mfs-loyalty-points-detail.component.html',
})
export class MfsLoyaltyPointsDetailComponent implements OnInit {
  mfsLoyaltyPoints: IMfsLoyaltyPoints | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ mfsLoyaltyPoints }) => (this.mfsLoyaltyPoints = mfsLoyaltyPoints));
  }

  previousState(): void {
    window.history.back();
  }
}
