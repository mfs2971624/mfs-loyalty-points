import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IMfsLoyaltyPoints, MfsLoyaltyPoints } from 'app/shared/model/mfs-loyalty-points.model';
import { MfsLoyaltyPointsService } from './mfs-loyalty-points.service';

@Component({
  selector: 'jhi-mfs-loyalty-points-update',
  templateUrl: './mfs-loyalty-points-update.component.html',
})
export class MfsLoyaltyPointsUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    customerMsisdn: [null, [Validators.required]],
    trxId: [null, [Validators.required]],
    pointsEarned: [],
    pointsUsed: [],
    isReset: [],
    previousBalance: [],
    currentBalance: [],
  });

  constructor(
    protected mfsLoyaltyPointsService: MfsLoyaltyPointsService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ mfsLoyaltyPoints }) => {
      this.updateForm(mfsLoyaltyPoints);
    });
  }

  updateForm(mfsLoyaltyPoints: IMfsLoyaltyPoints): void {
    this.editForm.patchValue({
      id: mfsLoyaltyPoints.id,
      customerMsisdn: mfsLoyaltyPoints.customerMsisdn,
      trxId: mfsLoyaltyPoints.trxId,
      pointsEarned: mfsLoyaltyPoints.pointsEarned,
      pointsUsed: mfsLoyaltyPoints.pointsUsed,
      isReset: mfsLoyaltyPoints.isReset,
      previousBalance: mfsLoyaltyPoints.previousBalance,
      currentBalance: mfsLoyaltyPoints.currentBalance,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const mfsLoyaltyPoints = this.createFromForm();
    if (mfsLoyaltyPoints.id !== undefined) {
      this.subscribeToSaveResponse(this.mfsLoyaltyPointsService.update(mfsLoyaltyPoints));
    } else {
      this.subscribeToSaveResponse(this.mfsLoyaltyPointsService.create(mfsLoyaltyPoints));
    }
  }

  private createFromForm(): IMfsLoyaltyPoints {
    return {
      ...new MfsLoyaltyPoints(),
      id: this.editForm.get(['id'])!.value,
      customerMsisdn: this.editForm.get(['customerMsisdn'])!.value,
      trxId: this.editForm.get(['trxId'])!.value,
      pointsEarned: this.editForm.get(['pointsEarned'])!.value,
      pointsUsed: this.editForm.get(['pointsUsed'])!.value,
      isReset: this.editForm.get(['isReset'])!.value,
      previousBalance: this.editForm.get(['previousBalance'])!.value,
      currentBalance: this.editForm.get(['currentBalance'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMfsLoyaltyPoints>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
