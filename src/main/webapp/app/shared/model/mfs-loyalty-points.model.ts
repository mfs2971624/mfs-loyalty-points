export interface IMfsLoyaltyPoints {
  id?: number;
  customerMsisdn?: string;
  trxId?: string;
  pointsEarned?: number;
  pointsUsed?: number;
  isReset?: boolean;
  previousBalance?: number;
  currentBalance?: number;
}

export class MfsLoyaltyPoints implements IMfsLoyaltyPoints {
  constructor(
    public id?: number,
    public customerMsisdn?: string,
    public trxId?: string,
    public pointsEarned?: number,
    public pointsUsed?: number,
    public isReset?: boolean,
    public previousBalance?: number,
    public currentBalance?: number
  ) {
    this.isReset = this.isReset || false;
  }
}
